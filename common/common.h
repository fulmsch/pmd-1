#ifndef COMMON_H
#define COMMON_H

#define F_CPU 16000000UL // 16 MHz

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#endif
