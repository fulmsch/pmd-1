#ifndef LCD_H
#define LCD_H

#include "common.h"

void lcd_init(void);
void lcd_setIO(void);
void lcd_setCursor(uint8_t column, uint8_t row);
void lcd_putc(char c);
void lcd_print(const char *str);
void lcd_number_clear(void);
void lcd_number(uint16_t n);

#endif // LCD_H
