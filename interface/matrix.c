#include "common.h"
#include "matrix.h"
#include "io.h"

static volatile uint32_t leds;
static volatile uint64_t milliseconds = 0;
static volatile uint8_t activeColumn = 0;
static volatile uint8_t buttonScanActive = 0;

uint64_t millis()
{
    return milliseconds;
}

void matrix_setIO()
{
    // Button rows as inputs with pullups
    DDRB &= ~0x1e;
    PORTB |= 0x1e;
}

void matrix_init()
{
    // Disable all columns by pulling them high
    PORTC = 0x3f;
    DDRC = 0x3f;

    // LED rows
    DDRD |= 0x3c;


    // Timer 0: Matrix scanning at 1 kHz
    TCCR0A = 0; // Normal operation
    TCCR0B = (1 << CS01) | (1 << CS00);  // 1/64 prescaler
    /* TIMSK0 = (1 << TOIE0); // Overflow interrupt */
    OCR0A = 250;
    TIMSK0 = (1 << OCIE0A); // Compare A interrupt
}

static void setOutputs()
{
    uint8_t col = activeColumn;

    // Disable LED rows to ensure clean transitions
    PORTD &= ~0x3c;

    // Pull active column low.
    // The only other pin in Port C is the reset pin, which is not an output,
    // so the entire register can be overwritten safely.
    PORTC = 0x3f & ~(1 << col);

    // Set LED rows
    PORTD |= 0x3c & ((leds >> 4 * col) << 2);
}

uint32_t matrix_getButtons(void)
{
    //TODO debouncing

    uint32_t buttons = 0;

    buttonScanActive = 1;

    // Disable LED rows
    PORTD &= ~0x3c;

    for (uint8_t i = 0; i < 6; i++) {
        PORTC = 0x3f & ~(1 << i);
        _delay_us(10);
        buttons |= (uint32_t)(~PINB & 0x1e) << (4 * i);
    }
    buttons >>= 1;

    setOutputs();

    buttonScanActive = 0;

    return buttons;
}

void matrix_setLeds(uint32_t l)
{
    leds = l;
}

/* ISR (TIMER0_OVF_vect) */
ISR (TIMER0_COMPA_vect)
{
    TCNT0 = 0;
    milliseconds++;

    if (++activeColumn > 5) {
        activeColumn = 0;
    }

    if (!buttonScanActive) {
        setOutputs();
    }
}
