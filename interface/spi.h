#ifndef SPI_H
#define SPI_H

#include "common.h"

void spi_setIO(void);
uint8_t spi_transfer(uint8_t data);


#endif // SPI_H
