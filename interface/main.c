#include "common.h"
#include "lcd.h"
#include "uart.h"
#include "programmer.h"
#include "spi.h"
#include "adc.h"
#include "matrix.h"
#include "io.h"


uint64_t last_tick = 0;
uint8_t play_mode = 0;
uint8_t write_mode = 0;
uint8_t step = 0;

uint64_t last_millis;

char *instrumentNames[16] = {
  "Sawtooth lo ",
  "Square lo   ",
  "Triangle lo ",
  "Sine lo     ",
  "Noise       ",
  "ModSquare lo",
  "            ",
  "            ",
  "Sawtooth hi ",
  "Square hi   ",
  "Triangle hi ",
  "Sine hi     ",
  "Noise       ",
  "ModSquare hi",
  "            ",
  "            ",
};


static uint8_t activeInstrument = 0;



// PD0 - PD1: UART
// PD2 - PD5: LED rows
// PD6: Slave select
// PD7: Slave reset
//
// PC0 - PC5: Columns
//
// PB0: LCD enable
//
// Buttons:
// PB1-PB4: buttons
//
// LCD:
// PB1-PB4: D4-D7
// PB5: RS
//
// SPI:
// PB3: MOSI
// PB4: MISO
// PB5: SCK




enum events {
  EV_NONE,

  EV_GLIDE_PRESSED,
  EV_FX_PRESSED,
  EV_PLAY_PRESSED,
  EV_WRITE_PRESSED,
  EV_BPM_PRESSED,
  EV_PATTERN_PRESSED,
  EV_SOUND_PRESSED,

  EV_GLIDE_RELEASED,
  EV_FX_RELEASED,
  EV_PLAY_RELEASED,
  EV_WRITE_RELEASED,
  EV_BPM_RELEASED,
  EV_PATTERN_RELEASED,
  EV_SOUND_RELEASED,

  EV_ADCA_CHANGED,
  EV_ADCB_CHANGED,

  EV_KEYPAD_PRESSED = 1 << 6,
  EV_KEYPAD_RELEASED = 1 << 7,
};

enum states {
  ST_IDLE,
  ST_PATTERN,
  ST_SOUND_SELECT,
  ST_BPM,
} state = ST_IDLE;

void handle_event(enum events ev) {
  if (ev == EV_PLAY_PRESSED) {
    if (play_mode) {
      play_mode = 0;
    } else {
      play_mode = 1;
    }
    return;
  } else if (ev == EV_WRITE_PRESSED) {
    write_mode = !write_mode;
    return;
  }

  static uint64_t last_sound_pressed;
  if (ev == EV_SOUND_PRESSED) {
    last_sound_pressed = millis();
  } else if (ev == EV_SOUND_RELEASED) {
    if ((millis() - last_sound_pressed) < 200) {
      // Toggle Octave
      activeInstrument = activeInstrument > 7 ? activeInstrument - 8 : activeInstrument + 8;

      io_setState(io_spi);
      PORTD &= ~(1 << PD6);
      spi_transfer(0xA0 | (activeInstrument & 0x0f));
      PORTD |= (1 << PD6);

      io_setState(io_lcd);
      lcd_setCursor(0, 0);
      lcd_print(instrumentNames[activeInstrument]);
      io_setState(io_buttons);
    }

  }

  switch (state) {
    case ST_IDLE:
      if (ev & EV_KEYPAD_PRESSED) {
        uint8_t note = ev & 0x0f;
        note = 4 * (3 - (note / 4)) + note % 4;

        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0x80 | note);
        PORTD |= (1 << PD6);
        io_setState(io_buttons);

        uart_puts("Note on: ");
        uart_printHex(note);
        uart_puts("\r\n");
      } else if (ev & EV_KEYPAD_RELEASED) {
        uint8_t note = ev & 0x0f;
        note = 4 * (3 - (note / 4)) + note % 4;

        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0x90 | note);
        PORTD |= (1 << PD6);
        io_setState(io_buttons);

        uart_puts("Note off: ");
        uart_printHex(note);
        uart_puts("\r\n");
      } else if (ev == EV_PATTERN_PRESSED) {
        state = ST_PATTERN;
      } else if (ev == EV_SOUND_PRESSED) {
        state = ST_SOUND_SELECT;
      } else if (ev == EV_SOUND_PRESSED) {
        state = ST_BPM;
      } else if (EV_ADCA_CHANGED) {
        uint8_t val = adcA_getVal();
        io_setState(io_lcd);
        lcd_number(val);

        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0xB0 | (activeInstrument & 0x0f));
        spi_transfer(adcA_getVal());
        PORTD |= (1 << PD6);
        io_setState(io_buttons);
      } else if (EV_ADCB_CHANGED) {
        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0xC0 | (activeInstrument & 0x0f));
        spi_transfer(adcB_getVal());
        PORTD |= (1 << PD6);
        io_setState(io_buttons);
      }
      break;
    case ST_PATTERN:
      if (ev == EV_PATTERN_RELEASED) {
        state = ST_IDLE;
      }
      break;
    case ST_SOUND_SELECT:
      if (ev & EV_KEYPAD_PRESSED) {
        uint8_t instrument = ev & 0x0f;
        activeInstrument = instrument;

        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0xA0 | (instrument & 0x0f));
        PORTD |= (1 << PD6);
        //io_setState(io_buttons);

        io_setState(io_lcd);
        lcd_setCursor(0, 0);
        lcd_print(instrumentNames[instrument]);
        io_setState(io_buttons);

        uart_puts("Instrument: ");
        uart_printHex(instrument);
        uart_puts("\r\n");
      } else if (ev == EV_SOUND_RELEASED) {
        state = ST_IDLE;
      }
      break;
    default:
      break;
  }

}

void interface_update()
{
  uint32_t buttons;
  static uint32_t prevButtons = 0;

  // Test: toggle LEDs on rising edge
  buttons = matrix_getButtons();
  uint32_t posedge = ~prevButtons & buttons;
  uint32_t negedge = prevButtons & ~buttons;
  /* matrix_setLeds(buttons); */

  if (posedge & (1UL <<  0)) handle_event(EV_GLIDE_PRESSED);
  if (posedge & (1UL <<  1)) handle_event(EV_FX_PRESSED);
  if (posedge & (1UL <<  2)) handle_event(EV_PLAY_PRESSED);
  if (posedge & (1UL <<  3)) handle_event(EV_WRITE_PRESSED);
  if (posedge & (1UL << 21)) handle_event(EV_BPM_PRESSED);
  if (posedge & (1UL << 22)) handle_event(EV_PATTERN_PRESSED);
  if (posedge & (1UL << 23)) handle_event(EV_SOUND_PRESSED);

  if (negedge & (1UL <<  0)) handle_event(EV_GLIDE_RELEASED);
  if (negedge & (1UL <<  1)) handle_event(EV_FX_RELEASED);
  if (negedge & (1UL <<  2)) handle_event(EV_PLAY_RELEASED);
  if (negedge & (1UL <<  3)) handle_event(EV_WRITE_RELEASED);
  if (negedge & (1UL << 21)) handle_event(EV_BPM_RELEASED);
  if (negedge & (1UL << 22)) handle_event(EV_PATTERN_RELEASED);
  if (negedge & (1UL << 23)) handle_event(EV_SOUND_RELEASED);

  if (posedge & 0xffff0) {
    for (uint8_t i = 0; i < 16; i++) {
      if (posedge & (0x10UL << i)) {
        uint8_t key = 4 * (i % 4) + (3 - (i / 4));
        handle_event(EV_KEYPAD_PRESSED | key);
      }
    }
  }

  if (negedge & 0xffff0) {
    for (uint8_t i = 0; i < 16; i++) {
      if (negedge & (0x10UL << i)) {
        uint8_t key = 4 * (i % 4) + (3 - (i / 4));
        handle_event(EV_KEYPAD_RELEASED | key);
      }
    }
  }

  if (adcA_hasChanged()) {
    handle_event(EV_ADCA_CHANGED);
  }

  if (adcB_hasChanged()) {
    handle_event(EV_ADCB_CHANGED);
  }

  /* uart_printHex(buttons >> 16); */
  /* uart_printHex(buttons >> 8); */
  /* uart_printHex(buttons); */
  /* uart_puts("\r\n"); */

  prevButtons = buttons;
}



int main()
{
  // Slave select and reset -> high
  DDRD |= (1 << PD6) | (1 << PD7);
  PORTD |= (1 << PD6) | (1 << PD7); // TODO reset slave

  io_setState(io_lcd);
  lcd_init();
  io_setState(io_buttons);


  adc_init();
  uart_init();
  matrix_init();

  sei();

  uart_puts("PMD-1\r\n> ");

  uint64_t last_interface_update = 0;

  while (1) {
    /* uint64_t millis = milliseconds; */

    if (play_mode) {
      if ((millis() - last_tick) >= (125 * 4)) {
        step++;
        if (step > 15) {
          step = 0;
        }

        uint8_t led =  step / 4 + 4 * (3 - step % 4);
        matrix_setLeds(0x10UL << led);

        last_tick = millis();

        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0x80 | 1);
        PORTD |= (1 << PD6);
        io_setState(io_buttons);

      } else if ((millis() - last_tick) >= 25) {
        io_setState(io_spi);
        PORTD &= ~(1 << PD6);
        spi_transfer(0x90 | 1);
        PORTD |= (1 << PD6);
        io_setState(io_buttons);

      }

    }


    if (millis() - last_interface_update > 10) {
      interface_update();
      last_interface_update = millis();
    }

    if (uart_available()) {
      char c = uart_getc();
      if (c == '*') {
        io_setState(io_spi);
        programmer_run();
        io_setState(io_buttons);
      } else {
        uart_putc(c);
      }
    }
  }
}
