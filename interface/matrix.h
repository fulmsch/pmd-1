#ifndef MATRIX_H
#define MATRIX_H

#include "common.h"

uint64_t millis(void);

void matrix_init(void);
void matrix_setIO(void);

uint32_t matrix_getButtons(void);
void matrix_setLeds(uint32_t leds);

#endif // MATRIX_H
