#include "common.h"
#include "io.h"
#include "matrix.h"
#include "lcd.h"
#include "spi.h"


static enum io_state io_state = io_buttons;

enum io_state io_getState()
{
    return io_state;
}

void io_setState(enum io_state s)
{
    SPCR = 0; // Disable SPI
    switch (s) {
        case io_buttons:
            DDRC = 0x3f;
            matrix_setIO();
            break;
        case io_lcd:
            DDRC = 0x00;
            lcd_setIO();
            break;
        case io_spi:
            DDRC = 0x00;
            spi_setIO();
            break;
        default:
            break;
    }
    io_state = s;
}
