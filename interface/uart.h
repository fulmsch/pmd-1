#ifndef UART_H
#define UART_H

#define BAUD 19200UL // UART baud rate

#include "common.h"

void uart_init(void);

uint8_t uart_available(void);
void uart_putc(uint8_t c);
uint8_t uart_getc(void);

void uart_puts(const char *string);

void uart_printHexDigit(uint8_t d);
void uart_printHex(uint8_t n);

#endif // UART_H
