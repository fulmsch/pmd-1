#include "adc.h"

volatile uint16_t rawValA, rawValB;
volatile uint16_t lastRawValA, lastRawValB;

ISR (ADC_vect)
{
  static uint8_t sampleCounter = 0;
  static uint16_t sumA, sumB;

  if (sampleCounter == 0) {
    sumA += ADC;
    rawValA = sumA >> 4;
    sumA = 0;

    ADMUX = (ADMUX & 0xf0) | 6;

  } else if (sampleCounter < 16) {
    sumB += ADC;

  } else if (sampleCounter == 16) {
    sumB += ADC;
    rawValB = sumB >> 4;
    sumB = 0;

    ADMUX = (ADMUX & 0xf0) | 7;

  } else {
    sumA += ADC;
  }

  sampleCounter++;
  if (sampleCounter >= 32) sampleCounter = 0;

  ADCSRA |= (1 << ADSC); // Start next conversion
}

void adc_init()
{
  ADMUX = (1 << REFS0) | 7;
  ADCSRA = (1 << ADEN) | (1 << ADSC)| (1 << ADIE) | // Enable ADC and Interrupt, start first conversion
           (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Prescaler 128 -> 125 kHz
}

bool adcA_hasChanged()
{
  return abs(lastRawValA - rawValA) > 3;
}

bool adcB_hasChanged()
{
  return abs(lastRawValB - rawValB) > 3;
}

uint8_t adcA_getVal()
{
  lastRawValA = rawValA;

  return rawValA >> 2;
}

uint8_t adcB_getVal()
{
  lastRawValB = rawValB;

  return rawValB >> 2;
}
