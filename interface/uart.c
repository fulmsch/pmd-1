#include "uart.h"
#include <util/setbaud.h>

#define RX_BUFFER_SIZE 128

static volatile uint8_t rxCount = 0;
static volatile uint8_t rxWriteIndex = 0;
static volatile uint8_t rxReadIndex = 0;
static volatile uint8_t rxBuffer[RX_BUFFER_SIZE];

ISR (USART_RX_vect)
{
  rxBuffer[rxWriteIndex] = UDR0;
  if (++rxWriteIndex >= RX_BUFFER_SIZE) rxWriteIndex = 0;
  if (rxCount < RX_BUFFER_SIZE - 1) rxCount++;
}

void uart_init()
{
  UBRR0H = UBRRH_VALUE;
  UBRR0L = UBRRL_VALUE;
  UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // Asynchronous 8n1
  UCSR0B = (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0); // Enable RX and TX, RX interrupt
}

void uart_putc(uint8_t c)
{
    // Wait for empty TX buffer
    while (!(UCSR0A & (1 << UDRE0))) {};
    // Send data
    UDR0 = c;
}

uint8_t uart_available()
{
  return rxCount > 0;
}

uint8_t uart_getc()
{
  uint8_t c;

  while (rxCount == 0) {};
  c = rxBuffer[rxReadIndex];
  if (++rxReadIndex >= RX_BUFFER_SIZE) rxReadIndex = 0;
  rxCount--;

  return c;
}

void uart_puts(const char *string)
{
  unsigned int i = 0;
  while (string[i] != '\0') {
    uart_putc(string[i]);
    i++;
  }
}

void uart_printHexDigit(uint8_t d)
{
  d &= 0x0f;
  uart_putc(d + (d > 9 ? 'A'-10 :'0'));
}

void uart_printHex(uint8_t n)
{
  uart_printHexDigit(n >> 4);
  uart_printHexDigit(n & 0x0f);
}
