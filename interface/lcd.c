#include "lcd.h"

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00


static void pulseEnable()
{
  PORTB &= ~(1 << PB0);
  _delay_us(1);
  PORTB |= (1 << PB0);
  _delay_us(1);    // enable pulse must be >450ns
  PORTB &= ~(1 << PB0);
  _delay_us(100);   // commands need > 37us to settle
}

static void writeNibble(uint8_t value)
{
  PORTB &= ~0x1e;
  PORTB |= (value & 0x0f) << 1;

  pulseEnable();
}

static void writeByte(uint8_t value)
{
  writeNibble(value >> 4);
  writeNibble(value);
}

static void sendCommand(uint8_t value)
{
  PORTB &= ~(1 << PB5);
  writeByte(value);
}

static void sendData(uint8_t value)
{
  PORTB |= (1 << PB5);
  writeByte(value);
}

void lcd_setIO()
{
  DDRB |= (1 << PB5) | 0x1e;
}

void lcd_init()
{
  /* // LCD enable */
  /* DDRB = (1 << PB0); */
  /* PORTB |= (1 << PB0); */

  PORTB |= (1 << PB0);
  DDRB |= (1 << PB0); // LCD enable
  _delay_ms(50);

  PORTB &= ~((1 << PB0) | (1 << PB5));

  writeNibble(0x03);
  _delay_ms(5);
  writeNibble(0x03);
  _delay_ms(5);
  writeNibble(0x03);
  _delay_ms(5);
  writeNibble(0x02);

  sendCommand(LCD_FUNCTIONSET | LCD_2LINE);
  sendCommand(LCD_DISPLAYCONTROL | LCD_DISPLAYON);
  sendCommand(LCD_CLEARDISPLAY);
  _delay_ms(2);
}

void lcd_setCursor(uint8_t column, uint8_t row) {
  uint8_t address = (column + row * 0x40) & 0x7f;
  sendCommand(LCD_SETDDRAMADDR | address);
}

void lcd_putc(char c) {
  sendData(c);
}

void lcd_print(const char *str) {
  unsigned int i = 0;
  while (str[i] != '\0') {
    sendData(str[i]);
    i++;
  }
}

void lcd_number_clear() {
  lcd_setCursor(13, 0);
  lcd_print("   ");
}

void lcd_number(uint16_t n) {
  uint8_t n1, n2, n3;
  n3 = n % 10;
  n /= 10;
  n2 = n % 10;
  n /= 10;
  n1 = n % 10;
  lcd_setCursor(13, 0);
  sendData(n1 + '0');
  sendData(n2 + '0');
  sendData(n3 + '0');
}
