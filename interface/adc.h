#ifndef ADC_H
#define ADC_H

#include "common.h"

void adc_init(void);

bool adcA_hasChanged(void);
bool adcB_hasChanged(void);

uint8_t adcA_getVal(void);
uint8_t adcB_getVal(void);


#endif // ADC_H
