#include "spi.h"

void spi_setIO()
{
  DDRB |= (1 << PB2) | // SS is unused but should be an output
          (1 << PB3) | (1 << PB5); // MOSI and SCK as outputs
  SPCR = (1 << SPE) | (1 << MSTR); // SPI enable as master
}

uint8_t spi_transfer(uint8_t data)
{
  SPDR = data;
  while (!(SPSR & (1 << SPIF))) {};
  return SPDR;
}
