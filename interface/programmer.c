#include "programmer.h"

#include "uart.h"
#include "spi.h"


static uint8_t spi_transaction(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
  spi_transfer(a);
  spi_transfer(b);
  spi_transfer(c);
  return spi_transfer(d);
}

// Flash: 256 pages, 64 words/page, 128 bytes/page
uint8_t pageBuffer[128];

static void programmingEnable()
{
  PORTD |= (1 << PD7);
  _delay_ms(1); // Positive reset pulse of at least 2 clock cycles
  PORTD &= ~(1 << PD7);
  _delay_ms(25); // Wait at least 20 ms

  spi_transfer(0xAC);
  spi_transfer(0x53);
  uint8_t reply = spi_transfer(0x00);
  spi_transfer(0x00);

  if (reply != 0x53) {
    // TODO error
    uart_putc('e');
  }
}

static uint8_t getLength()
{
  // Clear RX buffer, since we need all 128 bytes for a page
  while (uart_available()) uart_getc();

  uart_putc('r'); // Ready
  uint8_t length = uart_getc();
  /* uart_printHex(length); */
  if (length > 64) {
    //TODO error
    uart_putc('e');
  }
  return length;

}

static void programPage(uint8_t page, uint8_t length)
{
  // PC starts sending data
  for (uint8_t i = 0; i < length; i++) {
    spi_transaction(0x40, 0x00, i, uart_getc()); // Low byte
    spi_transaction(0x48, 0x00, i, uart_getc()); // High byte
  }
  uint16_t addr = page * 64;
  spi_transaction(0x4C, addr >> 8, addr & 0xff, 0x00);
  _delay_ms(10); // Minimum 4.5 ms

}


uint8_t programmer_run()
{
  programmingEnable();

  spi_transaction(0xAC, 0x80, 0x00, 0x00); // Chip erase
  _delay_ms(15); // Minimum 9 ms

  programmingEnable();

  uint8_t page = 0;
  uint8_t length;
  while ((length = getLength()) != 0) {
    uart_putc('k'); // Acknowledge

    programPage(page, length);
    page++;
  }

  PORTD |= (1 << PD7);

  return 0;
}
