#ifndef IO_H
#define IO_H

enum io_state {
  io_buttons,
  io_lcd,
  io_spi,
};

enum io_state io_getState(void);
void io_setState(enum io_state s);


#endif // IO_H
