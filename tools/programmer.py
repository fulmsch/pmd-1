#!/usr/bin/env python3

import serial
import sys

binary = open(sys.argv[1], 'rb')

ser = serial.Serial('/dev/ttyUSB0', 19200, timeout=5)  # open serial port

if ser.read_until(b'> ')[-2:] != b'> ':
    sys.exit()

ser.write(b'*') # Enter programming mode

bytes_written = 0
pages_written = 0

while True:
    print('waiting')
    if ser.read() != b'r':
        sys.exit("Error")
    print('AVR ready')

    page = binary.read(128)
    length = len(page)

    print(length)
    ser.write(int(length / 2).to_bytes(1, 'little'))

    if length == 0:
        print("Bytes:", bytes_written)
        print("Pages:", pages_written)
        quit()

    if ser.read() != b'k':
        sys.exit("error")
    print('Writing', length, 'bytes to page', pages_written)

    ser.write(page)
    bytes_written = bytes_written + length
    pages_written = pages_written + 1
