#!/usr/bin/env python3

import math

def add_table(name, length, function):
    print(name + '[' + str(length) + '] = {', end='')
    for i in range(length):
        print(function(i), end='')
        if i < length - 1:
            print(', ', end='')
    print('};')


def increment_function(i):
    frame_freq = 31250
    max_count = 2 ** 16

    # Whole notes from C4 to D6
    # frequencies = [261.626, 293.665, 329.628, 349.228, 391.995, 440, 493.883, 523.251, 587.33, 659.255, 698.456, 783.991, 880, 987.767, 1046.5, 1174.66]

    # 2 Octaves of A minor scale + G#
    # frequencies = [220.000, 246.942, 261.626, 293.665, 329.628, 349.228, 391.995, 415.305, 440.000, 493.883, 523.251, 587.330, 659.255, 698.456, 783.991, 830.609]

    # 3 Octaves of A minor scale + G#
    frequencies = [110.000, 123.471, 130.813, 146.832, 164.814, 174.614, 195.998, 207.652, 220.000, 246.942, 261.626, 293.665, 329.628, 349.228, 391.995, 415.305, 440.000, 493.883, 523.251, 587.330, 659.255, 698.456, 783.991, 830.609]



    return round(frequencies[i] * max_count / frame_freq)


def sine_function(i):
    return round (127 * math.sin(i * math.pi / 128))


def triangle_function(i):
    if i < 64:
        return round(i * (127 / 64))
    elif i < 192:
        return round(254 - i * (127 / 64))
    else:
        return round(-508 + i * (127 / 64))


print('#ifndef TABLES_H')
print('#define TABLES_H')
print('')

add_table('uint16_t increments', 24, increment_function)

print('')

add_table('int8_t sineTable', 256, sine_function)
add_table('int8_t triangleTable', 256, triangle_function)

print('')
print('#endif //TABLES_H')
