#include "common.h"
#include "tables.h"

#include <stdlib.h>

struct channel {
  uint8_t enable;
  uint8_t note;
  uint8_t instrument;
  uint16_t phase;
  uint8_t data[11];
};

struct channel channels[4];

uint16_t lfoIncrement = 256;

#define CMD_BUFFER_LEN 32
volatile uint8_t cmd_buffer[CMD_BUFFER_LEN];
volatile uint8_t cmd_buffer_head = 0;
volatile uint8_t cmd_buffer_tail = 0;

void parseCommands()
{
  uint8_t command, arg;
  while (cmd_buffer_head != cmd_buffer_tail) {
    command = cmd_buffer[cmd_buffer_head];
    if (cmd_buffer_head >= (CMD_BUFFER_LEN - 1)) {
      cmd_buffer_head = 0;
    } else {
      cmd_buffer_head++;
    }

    switch (command & 0xf0) {
      case 0x80:
        for (uint8_t i = 0; i < 4; i++) {
          if (channels[i].enable == 0) {
            channels[i].enable = 1;
            channels[i].note = command & 0x0f;
            channels[i].phase = 0;
            break;
          }
        }
        break;
      case 0x90:
        for (uint8_t i = 0; i < 4; i++) {
          if ((command & 0x0f) == channels[i].note) {
            channels[i].enable = 0;
            break;
          }
        }
        break;
      case 0xA0:
        // Instrument change
        for (uint8_t i = 0; i < 4; i++) {
          channels[i].instrument = command & 0x0f;
        }
        break;
      case 0xB0:
        arg = cmd_buffer[cmd_buffer_head];
        lfoIncrement = 110 + ((uint16_t)arg * 5);
        if (cmd_buffer_head >= (CMD_BUFFER_LEN - 1)) {
          cmd_buffer_head = 0;
        } else {
          cmd_buffer_head++;
        }
        break;
      case 0xC0:
        break;
      default:
        break;
    }

  }

}


ISR (SPI_STC_vect)
{
  cmd_buffer[cmd_buffer_tail] = SPDR;
  if (cmd_buffer_tail >= (CMD_BUFFER_LEN - 1)) {
    cmd_buffer_tail = 0;
  } else {
    cmd_buffer_tail++;
  }
}



volatile uint8_t pwmValue;
volatile uint8_t frameComplete;

// Triggers at 62.5 kHz
ISR (TIMER0_OVF_vect)
{
  static uint8_t divider = 0;

  // Skip every other interrupt
  if ((divider = !divider)) return;

  // New frames at 31.25 kHz
  OCR0A = pwmValue;
  frameComplete = 1;
}

uint32_t lfoPhase = 0;
uint8_t lfoVal;

int8_t getChannelVal(struct channel *channel)
{
  int8_t ddsValue;

  if (!channel->enable) {
    return 0;
  }

  uint8_t index = channel->phase >> 8;
  if (channel->instrument < 8) {
    channel->phase += increments[channel->note];
  } else {
    channel->phase += increments[channel->note + 8];
  }

  switch (channel->instrument) {
    case 0:
    case 8:
      ddsValue = index - 127;
      break;
    case 1:
    case 9:
      ddsValue = index > 0x7f ? -127 : 127;
      break;
    case 2:
    case 10:
      ddsValue = triangleTable[index];
      break;
    case 3:
    case 11:
      ddsValue = sineTable[index];
      break;
    case 4:
    case 12:
      ddsValue = rand() & 0xff;
      break;
    case 5:
    case 13:
      ddsValue = index > lfoVal ? -127 : 127;
      break;
    default:
      ddsValue = 0;
      break;
  }

  return ddsValue;
}

int main()
{
  DDRB |= (1 << PB4);
  SPCR = (1 << SPIE) | (1 << SPE);

  DDRD |= (1 << PD6);
  PORTD |= (1 << PD6);

  TCCR0A = (1 << COM0A1) | (1 << WGM01) | (1 << WGM00);
  TCCR0B = (1 << CS00);
  TIMSK0 = (1 << TOIE0);


  srand(4);

  sei();

  while (1) {
    lfoPhase += lfoIncrement;
    uint8_t lfoIndex = lfoPhase >> 16;
    lfoVal = 128 + (((uint16_t) sineTable[lfoIndex] * 5) >> 3);

    int16_t sum = 0;
    for (uint8_t i = 0; i < 4; i++) {
      sum += (int16_t)getChannelVal(&channels[i]);
    }
    sum = sum / 4;

    pwmValue = sum + 127;

    parseCommands();
    while (!frameComplete) {};
    frameComplete = 0;
  }
}
